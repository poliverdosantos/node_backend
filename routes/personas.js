const {Router} = require('express')
const {
  personaGet,
  personaPost,
  personaPut
} = require('../controllers/personas')


const route = Router()

route.get('/', personaGet)
route.post('/', personaPost)
route.put('/:id', personaPut)

module.exports = route