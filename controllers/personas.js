const { response , request} = require('express')

/*
const lista = [
  {
    nombre: "charly",
    edad: 23,
    domicilio: "cbba"
  },
  {
    nombre: "ever",
    edad: 23,
    domicilio: "cbba"
  }
]
*/



const personaGet = (req, res = response) => {

  res.json({
    msg: 'datos :',
    lista
  })
}

const personaPost = (req = request, res = response) => {
  const { nombre, edad, domicilio } = req.body;

  let nuevaPersona = { nombre ,edad , domicilio}
  lista.push(nuevaPersona)

  res.json({
    msg: 'datos post :',
    lista
  })
}
const personaPut = (req = request, res = response) => {
  const {id} = req.params

  const { nombre, edad, domicilio } = req.body;

  let nuevaPersona = { nombre ,edad , domicilio}

  res.json({
    msg: 'datos put :',
    id,
    nuevaPersona
  })
}

module.exports = {
  personaGet,
  personaPost,
  personaPut
}